package com.lg.internetbanking.controller;

import com.lg.internetbanking.dto.ClientDTO;
import com.lg.internetbanking.service.ClientServiceImpl;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/clients")
@AllArgsConstructor
public class ClientController {

    private final ClientServiceImpl clientService;

    @GetMapping
    public List<ClientDTO> listClients() {
        return clientService.listAllClients();
    }

    @GetMapping("/{clientId}")
    public ClientDTO showClientById(@PathVariable Long clientId) {
        return new ClientDTO(clientService.listClientById(clientId));
    }


}
