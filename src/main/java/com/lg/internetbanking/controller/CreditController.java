package com.lg.internetbanking.controller;

import com.lg.internetbanking.dto.CreditDTO;
import com.lg.internetbanking.model.Credit;
import com.lg.internetbanking.service.CreditServiceImpl;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/clients/{clientId}/credit")
public class CreditController {

    private final CreditServiceImpl creditService;

    @GetMapping
    public List<CreditDTO> listAll() {
        return creditService.listAll();
    }

    @PostMapping("/request")
    @ResponseStatus(HttpStatus.CREATED)
    public Credit save(@Valid @RequestBody Credit credit, @PathVariable Long clientId) {
        return creditService.save(credit, clientId);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable Long id) {
        creditService.delete(id);
    }


}
