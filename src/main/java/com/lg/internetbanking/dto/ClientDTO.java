package com.lg.internetbanking.dto;

import com.lg.internetbanking.model.Client;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ClientDTO {

    private Long id;
    private String name;
    private Integer score;
    private BigDecimal availableCredit;
    private List<CreditDTO> solicitations;

    public ClientDTO(Client client) {
        id = client.getId();
        name = client.getName();
        score = client.getScore();
        availableCredit = client.getAvailableCredit();
        solicitations = client.getSolicitations().stream().map(CreditDTO::new).collect(Collectors.toList());
    }
}
