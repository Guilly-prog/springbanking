package com.lg.internetbanking.dto;

import com.lg.internetbanking.model.Credit;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class CreditDTO {

    private Long id;
    private String clientName;
    private BigDecimal loanValue;
    private Integer installmentQuantity;
    private Boolean approved;
    private LocalDate requisitionDate;
    private List<InstallmentDTO> installments;


    public CreditDTO(Credit credit) {
        id = credit.getId();
        clientName = credit.getClientName();
        loanValue = credit.getLoanValue();
        installmentQuantity = credit.getInstallmentQuantity();
        approved = credit.getApproved();
        requisitionDate = credit.getRequisitionDate();
        installments = credit.getInstallments().stream().map(InstallmentDTO::new).collect(Collectors.toList());
    }

}
