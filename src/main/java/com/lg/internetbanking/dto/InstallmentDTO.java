package com.lg.internetbanking.dto;

import com.lg.internetbanking.model.Installment;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDate;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class InstallmentDTO {

    private Long id;
    private Boolean paid;
    private LocalDate dueDate;
    private BigDecimal value;

    public InstallmentDTO(Installment installment) {
        id = installment.getId();
        paid = installment.getPaid();
        dueDate = installment.getDueDate();
        value = installment.getValue();
    }
}
