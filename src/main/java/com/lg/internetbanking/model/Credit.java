package com.lg.internetbanking.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Credit {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private String clientName;

    @NotNull(message = "Please inform the loan value")
    private BigDecimal loanValue;

    @NotNull(message = "Please inform the installment quantity")
    private Integer installmentQuantity;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Boolean approved;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private LocalDate requisitionDate = LocalDate.now();

    @OneToMany(cascade = CascadeType.ALL)
    private List<Installment> installments = new ArrayList<>();

    @ManyToOne
    @JsonIgnore
    private Client client;

}
