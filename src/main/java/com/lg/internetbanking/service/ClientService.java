package com.lg.internetbanking.service;

import com.lg.internetbanking.dto.ClientDTO;
import com.lg.internetbanking.model.Client;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ClientService {

    List<ClientDTO> listAllClients();

    Client listClientById(Long id);
}
