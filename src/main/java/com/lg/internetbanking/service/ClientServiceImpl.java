package com.lg.internetbanking.service;

import com.lg.internetbanking.dto.ClientDTO;
import com.lg.internetbanking.model.Client;
import com.lg.internetbanking.model.ClientRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class ClientServiceImpl implements ClientService{

    private final ClientRepository clientRepository;


    @Override
    public List<ClientDTO> listAllClients() {
        return clientRepository.findAll().stream().map(ClientDTO::new).collect(Collectors.toList());
    }

    @Override
    public Client listClientById(Long id) {
        return clientRepository.findById(id).orElseThrow();
    }
}
