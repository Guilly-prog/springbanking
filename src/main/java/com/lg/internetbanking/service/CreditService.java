package com.lg.internetbanking.service;

import com.lg.internetbanking.dto.CreditDTO;
import com.lg.internetbanking.model.Credit;

import java.util.List;

public interface CreditService {

    public List<CreditDTO> listAll();

    public Credit save(Credit creditDTO, Long clientId);

    public void delete(Long id);


}
