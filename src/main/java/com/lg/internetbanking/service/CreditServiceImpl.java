package com.lg.internetbanking.service;

import com.lg.internetbanking.dto.ClientDTO;
import com.lg.internetbanking.dto.CreditDTO;
import com.lg.internetbanking.model.Client;
import com.lg.internetbanking.model.Credit;
import com.lg.internetbanking.model.CreditRepository;
import com.lg.internetbanking.utils.ApproveCredit;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@Service
public class CreditServiceImpl implements CreditService {

    private CreditRepository creditRepository;
    private ClientServiceImpl clientService;

    @Override
    public List<CreditDTO> listAll() {
        return creditRepository.findAll().stream().map(CreditDTO::new).collect(Collectors.toList());
    }

    @Override
    public Credit save(Credit credit, Long clientId) {
        Client client = clientService.listClientById(clientId);

        credit.setClientName(client.getName());
        ApproveCredit.approve(credit, client);
        if (credit.getApproved()) {
            ApproveCredit.generateInstallments(credit);
        }
        client.getSolicitations().add(credit);
        return creditRepository.save(credit);
    }

    @Override
    public void delete(Long id) {
        creditRepository.deleteById(id);
    }
}
