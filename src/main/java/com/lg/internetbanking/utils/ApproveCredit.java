package com.lg.internetbanking.utils;

import com.lg.internetbanking.model.Client;
import com.lg.internetbanking.model.Credit;
import com.lg.internetbanking.model.Installment;

import java.math.BigDecimal;
import java.time.LocalDate;

public class ApproveCredit {

    private static final Integer MINIMUM_SCORE = 300;
    private static final BigDecimal TAX_RATE = new BigDecimal("1.5");

    private ApproveCredit() {
    }

    public static void approve(Credit credit, Client client) {
        Integer score = client.getScore();
        BigDecimal availableCredit = client.getAvailableCredit();
        BigDecimal loanValue = credit.getLoanValue();
        int result = loanValue.compareTo(availableCredit);
        //1 -> loan > available
        //0 -> loan == available
        //-1 -> loan < available

        Boolean scoreResult = score >= MINIMUM_SCORE;
        Boolean valueResult = (result != 1);
        credit.setApproved(scoreResult && valueResult);
    }

    public static void generateInstallments(Credit credit) {
        for (int i = 1; i <= credit.getInstallmentQuantity(); i++) {
            LocalDate due = credit.getRequisitionDate().plusMonths(i);
            BigDecimal installmentValue = calculateInstallments(TAX_RATE, credit.getInstallmentQuantity(), credit.getLoanValue());
            Installment installment = new Installment(due, installmentValue);
            credit.getInstallments().add(installment);
        }
    }

    public static BigDecimal calculateInstallments(BigDecimal taxRate, Integer installmentQty, BigDecimal loanValue) {

        BigDecimal installmentValue;
        BigDecimal installmentTax;
        BigDecimal taxRatePercent = taxRate.divide(new BigDecimal("100"));
        BigDecimal taxRatePercentPlusOne = taxRatePercent.add(new BigDecimal("1"));

        // Considerando @param taxaJuros ao m�s e parcelas fixas
        // https://mundoeducacao.uol.com.br/matematica/tabela-price.htm

        installmentTax = BigDecimal.valueOf((Math.pow(taxRatePercentPlusOne.doubleValue(), installmentQty) * taxRatePercent.doubleValue())
                / (Math.pow(taxRatePercentPlusOne.doubleValue(), installmentQty) - 1));

        installmentValue = loanValue.multiply(installmentTax);
        String formattedValue = FormatUtils.formatDecimal(installmentValue);
        return new BigDecimal(formattedValue);
    }
}
